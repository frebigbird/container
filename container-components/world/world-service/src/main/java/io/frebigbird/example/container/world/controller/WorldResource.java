package io.frebigbird.example.container.world.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/world")
@RequiredArgsConstructor
public class WorldResource {
    @GetMapping
    public String find() {
        return "world";
    }
}
