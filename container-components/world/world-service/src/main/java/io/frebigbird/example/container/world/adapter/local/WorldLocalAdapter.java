package io.frebigbird.example.container.world.adapter.local;

import io.frebigbird.example.container.world.adapter.WorldAdapter;
import io.frebigbird.example.container.world.service.WorldService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@RequiredArgsConstructor
public class WorldLocalAdapter implements WorldAdapter {
    private final WorldService worldService;

    @Override
    public String find() {
        return worldService.find();
    }
}
