package io.frebigbird.example.container.world.service;

import io.frebigbird.example.container.world.domain.World;
import org.springframework.stereotype.Service;

@Service
public class WorldService {
    public String find() {
        return new World().toString();
    }
}
