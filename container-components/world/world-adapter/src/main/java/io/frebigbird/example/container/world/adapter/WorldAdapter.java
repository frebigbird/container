package io.frebigbird.example.container.world.adapter;

public interface WorldAdapter {
    String find();
}
