package io.frebigbird.example.container.world.domain;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class World {
    public String toString() {
        return "world";
    }
}
