package io.frebigbird.example.container.hello.adapter;

public interface HelloAdapter {
    String find();
}
