package io.frebigbird.example.container.hello.service;

import io.frebigbird.example.container.hello.domain.Hello;
import org.springframework.stereotype.Service;

@Service
public class HelloService {
    public String find() {
        return new Hello().toString();
    }
}
