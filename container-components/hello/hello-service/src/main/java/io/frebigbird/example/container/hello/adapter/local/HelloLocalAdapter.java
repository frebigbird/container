package io.frebigbird.example.container.hello.adapter.local;

import io.frebigbird.example.container.hello.adapter.HelloAdapter;
import io.frebigbird.example.container.hello.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@RequiredArgsConstructor
public class HelloLocalAdapter implements HelloAdapter {
    private final HelloService helloService;

    @Override
    public String find() {
        return helloService.find();
    }
}
