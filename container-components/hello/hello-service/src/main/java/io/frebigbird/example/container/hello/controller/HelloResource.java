package io.frebigbird.example.container.hello.controller;

import io.frebigbird.example.container.hello.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
public class HelloResource {
    private final HelloService helloService;

    @GetMapping
    public String find() {
        return helloService.find();
    }
}
