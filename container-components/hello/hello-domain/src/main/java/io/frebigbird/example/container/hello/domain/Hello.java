package io.frebigbird.example.container.hello.domain;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Hello {
    public String toString() {
        return "hello";
    }
}
