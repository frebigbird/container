package io.frebigbird.example.container.helloworld.service;


import io.frebigbird.example.container.hello.adapter.HelloAdapter;
import io.frebigbird.example.container.world.adapter.WorldAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class HelloworldService {
    private final HelloAdapter helloAdapter;

    private final WorldAdapter worldAdapter;

    public String find() {
        return helloAdapter.find() + " " + worldAdapter.find();
    }
}
