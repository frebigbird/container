package io.frebigbird.example.container.helloworld.controller;

import io.frebigbird.example.container.helloworld.service.HelloworldService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/printer")
@RequiredArgsConstructor
public class HelloworldResource {
    private final HelloworldService printerService;

    @GetMapping
    public String find() {
        return printerService.find();
    }
}
